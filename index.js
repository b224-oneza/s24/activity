// alert("Hello World!");
// 3

let num1 = 2;
const getCube = num1 ** 3;

// 4

let message = `The cube of ${num1} is`;
console.log(`${message} ${getCube}`);

// 5

const fullAddress = ["258", "Washington Ave. NW", "California 90011"]

// 6

const [houseNumber, city, countryWithZip] = fullAddress
console.log(`I live at ${houseNumber} ${city}, ${countryWithZip}`)

// 7

const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	size: "20 ft 3 in"
}

//8 

const { name, type, weight, size} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${size}.`)

// 9

const numbers = [1, 2, 3, 4, 5, 15,];

// 10

numbers.forEach((number) => {
	console.log(`${number}`)
});

// 11

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};

// 12

const myDog = new Dog ("Frankie", "5", "Miniature Dachsund")
console.log(myDog);

const myFirstDog = new Dog ("Harley", "2", "German Shepherd")
console.log(myFirstDog);

const myNewDog = new Dog ("Marshall", "1", "Pomeranian")
console.log(myNewDog);